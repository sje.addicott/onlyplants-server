<?php

# Function to connect to the database
function OpenCon()
{
    $dbhost = "localhost";
    $dbuser = "dev";
    $dbpass = "pass";
    $db = "onlyplants";
    $conn = new mysqli($dbhost, $dbuser, $dbpass,$db) or die("Connect failed: %s\n". $conn -> error);
    
    return $conn;
}

function CloseCon($conn)
{
    $conn -> close();
}

?>