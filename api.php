<?php

include "database.php";

$conn = OpenCon();

switch ($_GET["q"]){
    case "all":
        $all_temp = "SELECT temp, humidity, datetime FROM plant ORDER BY datetime DESC LIMIT 10;";
        $result = $conn->query($all_temp);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $output[] = $row;
            }
        }
        header("content-type:application/json");
        echo json_encode($output,JSON_PRETTY_PRINT);
    break;
    
    case "recent":
        $current_temp = "SELECT temp, humidity, datetime FROM plant ORDER BY datetime DESC LIMIT 1";
        $result = $conn->query($current_temp);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $output[] = $row;
            }
        }
        header("content-type:application/json");
        echo json_encode($output,JSON_PRETTY_PRINT);
    break;
}

CloseCon($conn);

?>